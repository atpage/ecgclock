from .compas import ReadTXTFile, ReadIcardiacFile, ReadTWBFile  # TODO?: correct_compas_loader, correct_txt_loader
from .csv import load_from_csv
