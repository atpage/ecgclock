
################################### Imports: ###################################

# Need to do this to prevent warnings/errors when saving a batch of clocks:
import matplotlib
matplotlib.use('Qt4Agg', warn=False)  # adds improved zoom over default backend
# matplotlib.use('GTKCairo', warn=False)  # switch to vector graphics
# matplotlib.use('GTKAgg', warn=False)  # nothing fancy over default.  similar to WXAgg.

import numpy as np
import datetime as dt

from .ecgclock import ECGClock

################################# Main Class: ##################################

class DeltaClock(ECGClock):
    def __init__(self, title=None,
                 min_rad=-500, max_rad=500,
                 autoscale=False,
                 color_cycle=['b', 'm', 'c', 'k', 'y'],
                 parent_figure=None,
                 subplot=1):
        """Create a plot (ECG Clock) that will be used to visualize long-term changes
        (deltas) in some measurement (e.g. QTc).
        """
        # Save args to pass on:
        locals_minus_self = locals()
        locals_minus_self.pop('self', None)

        #super(QTClock, self).__init__( **locals_minus_self )  # python 2
        #super().__init__( **locals_minus_self )  # python 3?
        ECGClock.__init__(self, **locals_minus_self )  # python 2 or 3, maybe

    def add_delta(self, rec1, rec2, column, recompute=False, label=None,
                  color=None, filtering=0, color_col=None, cmap=None,
                  autocorrect_time_units=False):
        """Compute delta in column from rec1 to rec2, and plot it. For example, rec1
        could be the baseline and rec2 is after an intervention, and you want to
        plot delta QTc but have only computed QTc.

        If you already have the delta as a column in a Recording, just add it to
        the plot using the normal add_recording() function.  We will attempt to
        do this automatically if a 'delta XYZ' column is found in rec2.
        """
        new_column_name = 'delta ' + column
        compute_deltas(rec1, rec2, [column], overwrite=recompute)
        self.add_recording(rec2, new_column_name, label=label, color=color,
                           filtering=filtering, color_col=color_col, cmap=cmap,
                           autocorrect_time_units=autocorrect_time_units)

    def highlight_range(self, rad_range, color, label=None):
        """Highlight range of radii a certain color.  zorder is set to -1 for this, so
        foreground items should use zorder>-1.

        Keyword arguments:
        rad_range -- range to highlight, e.g. [0, 1000] or [[lowers ...], [uppers ...]]
        color     -- color to use for highlighting
        label     -- what to call this highlight on the legend
        """
        theta = np.linspace(0, 2*np.pi, 100)  # 100 may not always be enough
        self.ax.fill_between(theta, rad_range[0], rad_range[1],
                             color=color, alpha=0.2, linewidth=0, zorder=-1,
                             label=label)

    def add_danger_range(self, rad_range, label='danger'):
        """Highlight a dangerous region in red.

        Keyword arguments:
        rad_range -- range to highlight, e.g. [0, 1000] or [[lowers ...], [uppers ...]]
        label     -- what to call this highlight on the legend
        """
        self.highlight_range(rad_range=rad_range, color='r', label=label)
        # TODO: handle axes being zoomed out in interactive window?

    def add_good_range(self, rad_range, label='good'):
        """Highlight a good region in green.

        Keyword arguments:
        rad_range -- range to highlight, e.g. [0, 1000] or [[lowers ...], [uppers ...]]
        label     -- what to call this highlight on the legend
        """
        self.highlight_range(rad_range=rad_range, color='g', label=label)
        # TODO: handle axes being zoomed out in interactive window?

# TODO?: kwargs

############################## Helper functions: ###############################

def round_time(t):
    """round time t down to nearest 10 seconds."""
    return dt.time(t.hour, t.minute, t.second - t.second%10)

def compute_deltas(rec1, rec2, columns, overwrite=False):
    """Add new column(s) to Recording #2 containing the change in some measurement(s)
    vs. Recording #1.  columns is a list of column names to computer deltas for.

    For example:

      compute_deltas(rec1, rec2, ['RR'])

    will add a 'delta RR' column to rec2.data which contains the (time-synched)
    result of rec2.data.RR - rec1.data.RR.

    Values will only be computed at times in rec2 that have corresponding
    measurements (within 10 seconds of same time of day) in rec1.
    """
    holter1, holter2 = rec1.data, rec2.data
    if not overwrite:
        for column in columns:
            new_column_name = 'delta ' + column
            if new_column_name in holter2.columns:
                columns.remove(column)
        if not columns:
            return
    intervals = [dt.time(h,m,s) for h in range(24)
                                for m in range(60)
                                for s in range(0,60,10)]
    holter1_rounded_times = np.array([round_time(t) for t in holter1.index.time])
    baseline = {}
    for column in columns:
        baseline[column] = np.full(len(intervals), np.nan)
        for i,interval in enumerate(intervals):
            relevant_values = holter1[holter1_rounded_times == interval][column]
            if len(relevant_values) > 0:
                baseline[column][i] = np.nanmedian(relevant_values)
    holter2_rounded_times = np.array([round_time(t) for t in holter2.index.time])
    interval_map = np.searchsorted(intervals, holter2_rounded_times)
    for column in columns:
        new_column_name = 'delta ' + column
        holter2[new_column_name] = holter2[column] - baseline[column][interval_map]

################################################################################
