#!/usr/bin/env python3

import unittest
import multiprocessing as mp
import os
import pkg_resources
import tempfile
import datetime as dt

from ecgclock import ECGFigure, ECGClock, QTClock, Recording, Cohort
import matplotlib.pyplot as plt

from ecgclock.utils import *

baseline_eg_filename = pkg_resources.resource_filename('ecgclock', 'example_data/baseline_eg.csv')
drug_eg_filename     = pkg_resources.resource_filename('ecgclock', 'example_data/drug_eg.csv')
eg_data_qtc_colname = 'filtered_QTcB'
tmp_dir = tempfile.gettempdir()

# TODO?: setUp() functions
# TODO?: switch floating point == checks to math.isclose() or something
# TODO: HRDerivClock test(s)
# TODO: test Recording.compute_qtc (including smoothed HR)... should change
# example data to QT and RR (instead of just qtc)
# TODO: test make_qtclock command w/various args

class TestUtils(unittest.TestCase):

    def test_time_converters(self):
        self.assertTrue( angle_to_time(np.pi/2) == '06:00' )
        self.assertTrue( time_to_angle('06:00') == np.pi/2 )
        self.assertTrue( sec_to_msec([ 5], cutoff=10) == [5000] )
        self.assertTrue( sec_to_msec([15], cutoff=10) == [  15] )
        self.assertTrue( msec_to_sec([ 5], cutoff=10) == [5    ] )
        self.assertTrue( msec_to_sec([15], cutoff=10) == [0.015] )
        # TODO:
        # datetimes = ...
        # times_to_angles(datetimes)
        # times_to_angles(times)

    # TODO:
    # def test_filter_functions(self):
    #     medfilt( times, values, filter_width=5 )
    #     general_filter( times, values, filter_width=5, filt_type=np.nanmedian )

    # TODO:
    # def test_other_utils(self):
    #     polar_interp( thetas, rs )
    #     derivative( times, values, percent=False )

class TestClocks(unittest.TestCase):

    def test_create_ECG_figure(self):
        plt.close('all')
        my_fig = ECGFigure(title='Test ECG Figure')

    def test_create_ECG_clock(self):
        plt.close('all')
        my_clock = ECGClock('Test ECG Clock')

    def test_create_QT_clock(self):
        plt.close('all')
        my_clock = QTClock('Test QT Clock')

    def test_percentiles(self):
        baseline_meas = Recording(filename = baseline_eg_filename)
        drug_meas     = Recording(filename = drug_eg_filename)
        cohort = Cohort(anns=[baseline_meas, drug_meas],
                        name='test_cohort')
        cohort.compute_pctls(eg_data_qtc_colname)
        outfile = os.path.join(tmp_dir,'ecgclock_pct_test.csv')
        before_save  = cohort.percentiles[eg_data_qtc_colname]
        # TODO: check before_save has correct values
        cohort.save_pctls(eg_data_qtc_colname, outfile)
        cohort.load_pctls(eg_data_qtc_colname, outfile)
        after_reload = cohort.percentiles[eg_data_qtc_colname]
        self.assertTrue( np.allclose(before_save.astype(float),
                                     after_reload.astype(float)) )

    def test_simple_clock(self):
        plt.close('all')
        my_clock = QTClock('QTcB: Baseline vs. Drug Trial')
        my_clock.add_recording(baseline_eg_filename, column=eg_data_qtc_colname, label='baseline')
        my_clock.add_recording(drug_eg_filename,     column=eg_data_qtc_colname, label='drug'    )
        my_clock.add_annotation('13:03', 476, 0.1, 0.05,
                                label='476ms @ 13:03')
        my_clock.add_default_ranges()
        my_clock.add_legend()
        my_clock.save(os.path.join(tmp_dir,'baseline_vs_drug.png'))
        print( "Saved example plot to %s.  You should make sure it worked." % \
               os.path.join(tmp_dir,'baseline_vs_drug.png') )
        # my_clock.show()  # view it in an interactive window

    def test_complex_clock(self):
        plt.close('all')
        my_fig = ECGFigure(nrows=1, ncols=2, title='QTcB: Baseline vs. Drug Trial')
        before_clock = QTClock('Combined', parent_figure=my_fig, subplot=1)
        after_clock =  QTClock('Drug',     parent_figure=my_fig, subplot=2)
        baseline_meas = Recording(filename=baseline_eg_filename)
        drug_meas     = Recording(filename=drug_eg_filename)
        before_clock.add_recording(baseline_meas, column=eg_data_qtc_colname, label='baseline')
        before_clock.add_recording(drug_meas,     column=eg_data_qtc_colname, label='drug')
        before_clock.add_danger_range()
        cohort = Cohort(anns=[baseline_meas, drug_meas])
        before_clock.add_percentile_range(
            source = cohort,
            field  = eg_data_qtc_colname,
            lower  =  1,
            upper  = 99,
            label  = 'full QTc range'
        )
        before_clock.add_legend()
        after_clock.add_recording(drug_meas, column=eg_data_qtc_colname, label='smoothed QTc')
        after_clock.add_recording(drug_meas, column=eg_data_qtc_colname, label='extra smoothed QTc', filtering=20)
        after_clock.add_annotation('13:03', 476, 0.6, 0.1,
                                   label='476ms @ 13:03')
        after_clock.add_default_ranges()
        after_clock.add_legend()
        my_fig.save(os.path.join(tmp_dir,'baseline_vs_drug_subplots.png'))
        print( "Saved example plot to %s.  You should make sure it worked." % \
               os.path.join(tmp_dir,'baseline_vs_drug_subplots.png') )
        # my_fig.show()  # view it in an interactive window

class TestLoaders(unittest.TestCase):

    def test_csv_loader(self):
        anns = Recording(filename=drug_eg_filename)

    # # TODO:
    # def test_twb_loader(self):
    #     anns = Recording(filename=TODO)
    # def test_txt_loader(self):
    #     anns = Recording(filename=TODO)
    # def test_icardiac_loader(self):
    #     anns = Recording(filename=TODO)
    # def test_gilead_loader(self):
    #     pass

if __name__ == '__main__':
    mp.freeze_support()
    unittest.main()
